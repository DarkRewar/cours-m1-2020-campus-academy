﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Destroy(this);
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Collect(this);
            //Destroy(gameObject);
        }
    }
}
