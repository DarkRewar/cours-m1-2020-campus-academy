﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTrigger : MonoBehaviour
{
    public Color ColorChangeEnter = Color.white;
    public Color ColorChangeExit = Color.white;

    private void OnTriggerEnter(Collider other)
    {
        // Sur Unity <2018
        Renderer renderer = other.GetComponent<Renderer>();
        if (renderer)
        {
            renderer.material.color = ColorChangeEnter;
        }

        // Sur Unity 2019+ (consomme moins)
        //if(other.TryGetComponent(out Renderer rend))
        //{
        //    rend.material.color = ColorChange;
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        Renderer renderer = other.GetComponent<Renderer>();
        if (renderer)
        {
            renderer.material.color = ColorChangeExit;
        }
    }
}
