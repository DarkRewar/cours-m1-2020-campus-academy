﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject BallToSpawn;
    public bool AttachToParent = false;

    public uint NumberBallToSpawn = 100;
    public float SecondsBetweenSpawns = 0.5f;

    // On peut déclarer le Start() comme étant une coroutine
    IEnumerator Start()
    {
        for(int i = 0; i < NumberBallToSpawn; ++i)
        {
            GameObject balle;
            if (AttachToParent)
            {
                balle = Instantiate(BallToSpawn, transform);
                balle.transform.position = transform.position;
            }
            else
            {
                balle = Instantiate(
                    BallToSpawn,
                    transform.position,
                    Quaternion.identity
                );
            }

            Destroy(balle, 10);
            yield return new WaitForSeconds(SecondsBetweenSpawns);
        }
    }
}
