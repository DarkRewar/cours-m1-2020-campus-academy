﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TPNote2
{
    [RequireComponent(typeof(Rigidbody))]
    public class Player : MonoBehaviour
    {
        public float JumpForce;

        public Rigidbody Rigidbody;

        public float PlayerSpeed = 1;

        private bool _jumpDone = true;

        private AudioSource _audioJump;

        // Start is called before the first frame update
        void Start()
        {
            if (!Rigidbody)
            {
                Rigidbody = GetComponent<Rigidbody>();
            }

            _audioJump = GetComponent<AudioSource>();
        }

        private void Update()
        {
            /**
             * Le joueur demande à sauter, on stocke alors la demande
             * en passant le "jumpDone" à false.
             */
            if ((Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)) && _jumpDone)
            {
                _jumpDone = false;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            // Si la partie est finie, tout s'arrête.
            if (GameManager.Instance.GameIsFinished) return;
            
            /**
             * On fait bouger le joueur vers la droite
             * directement en appliquant le MovePosition.
             */
            Vector3 direction = Time.fixedDeltaTime * PlayerSpeed * Vector3.right;
            Rigidbody.MovePosition(Rigidbody.position + direction);

            /**
             * Si le joueur demande à sauter, on récupère
             * la vélocité actuelle du Rigidbody, et on
             * modifie directement la valeur Y pour avoir
             * un saut instantané.
             */
            if (!_jumpDone)
            {
                _jumpDone = true;
                Vector3 vel = Rigidbody.velocity;
                vel.y = JumpForce;
                Rigidbody.velocity = vel;
                _audioJump.Play();
            }

            /**
             * BONUS
             * On oriente le joueur dans la direction de sa chute
             * ou de son saut. On sécurise en capant la rotation entre
             * -40° et 40°.
             */
            float rot = Mathf.Clamp(Rigidbody.velocity.y * 40/JumpForce, -40, 40);
            Rigidbody.MoveRotation(Quaternion.Euler(rot * Vector3.forward));
        }

        private void OnCollisionEnter(Collision collision)
        {
            /**
             * Lorsque le joueur entre en collision avec n'importe
             * quoi, la partie est finie.
             */
            GameManager.Instance.EndGame();
            gameObject.SetActive(false);
        }

        private void OnTriggerExit(Collider other)
        {
            /**
             * Lorsque le joueur a dépassé, et est donc sorti
             * du trigger de score, alors on marque le point
             */
            GameManager.Instance.Scored();
        }
    }
}