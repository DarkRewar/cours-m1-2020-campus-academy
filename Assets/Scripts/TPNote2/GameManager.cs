﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TPNote2
{
    public class GameManager : MonoBehaviour
    {
        #region SINGLETON

        public static GameManager Instance;

        public void Awake()
        {
            if (Instance) Destroy(gameObject);
            else Instance = this;
        }

        #endregion

        public int Score;
        public TMP_Text[] ScoreTexts;
        public GameObject EndGamePanel;

        public Camera Camera;
        public GameObject ObstaclePrefab;

        /// <summary>
        /// La range aléatoire où doivent apparaître les
        /// obstacles en Y.
        /// </summary>
        public Vector2 ObstacleRange = new Vector2(-2, 3);

        /// <summary>
        /// Est-ce que la partie est finie ?
        /// </summary>
        private bool _gameIsFinished = false;

        /// <summary>
        /// Il s'agit d'un getter readonly en C#. Il retourne
        /// simplement la valeur mais ne peut pas la modifier.
        /// </summary>
        public bool GameIsFinished => _gameIsFinished;

        /// <summary>
        /// Le cooldown, en seconde, entre deux apparitions
        /// d'obstacles.
        /// </summary>
        public float SpawnRate = 1;

        /// <summary>
        /// Le cooldown de la précédente apparition.
        /// </summary>
        private float _lastSpawn = 0;

        /// <summary>
        /// La liste de tous les obstacles en jeu.
        /// </summary>
        private List<GameObject> _obstacles = new List<GameObject>();

        /// <summary>
        /// BONUS
        /// La liste des obstacles disponibles pour être réutilisés.
        /// </summary>
        private Queue<GameObject> _obstaclesAvailable = new Queue<GameObject>();

        public AudioClip EndGameMusic;
        private AudioSource _musicSource;

        // Use this for initialization
        void Start()
        {
            Camera = Camera.main;
            _musicSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_gameIsFinished) return;

            /**
             * On vérifie si le cooldown d'apparition
             * des obstacles est terminé. Si oui, on appelle
             * le spawn, sinon on décrémente le cooldown.
             */
            if(_lastSpawn <= 0)
            {
                SpawnObstacle();
            }
            else
            {
                _lastSpawn -= Time.deltaTime;
            }

            /**
             * BONUS
             * Ici, je veux éviter d'appeler sans cesse le Destroy()
             * quand un obstacle sort de l'écran. Cela évite de surcharger la mémoire.
             * Alors, je fais du recyclage et je réutilise les obstacles dont
             * je ne me sers plus.
             * 
             * Je parcours la liste de tous les obstacles en jeu et si un
             * obstacle est sorti de mon champ de vision, alors j'indique
             * qu'il peut être réutilisé en l'ajoutant dans une file d'attente.
             */
            for (int i = _obstacles.Count - 1; i >= 0; --i)
            {
                GameObject obs = _obstacles[i];
                if (obs.transform.position.x < (Camera.transform.position.x - 10))
                {
                    _obstaclesAvailable.Enqueue(obs);
                    _obstacles.RemoveAt(i);
                }
            }
        }

        private void SpawnObstacle()
        {
            // On initialise le cooldown.
            _lastSpawn = SpawnRate;

            /**
             * On calcule la position de la prochaine apparition
             * de l'obstacle par rapport à la position de la
             * caméra.
             * 
             * La hauteur de l'obstacle est définie aléatoirement.
             */
            Vector3 pos = new Vector3(
                Camera.transform.position.x + 10,
                Random.Range(ObstacleRange.x, ObstacleRange.y),
                0
            ); 
            
            /**
             * Si j'ai des obstacles disponibles dans ma file d'attente,
             * alors je demande à en récupérer un. Cela m'évite d'en 
             * instancier un nouveau et de gaspiller de la RAM.
             * Sinon, j'en instancie un.
             */
            GameObject obs;
            if (_obstaclesAvailable.Count > 0)
            {
                obs = _obstaclesAvailable.Dequeue();
                obs.transform.position = pos;
            }
            else
            {
                obs = Instantiate(ObstaclePrefab, pos, Quaternion.identity);
            }

            /**
             * Le nouvel obstacle obtenu, soit depuis la Queue, soit
             * depuis le Instantiate, s'inscrit dans la liste de tous 
             * les obstacles actuellement en jeu.
             */
            _obstacles.Add(obs);
        }

        /// <summary>
        /// Incrémente le score et met à jour tous les TMP_Text
        /// qui doivent afficher le score.
        /// </summary>
        public void Scored()
        {
            if (_gameIsFinished) return;

            ++Score;
            foreach(TMP_Text text in ScoreTexts)
            {
                text.text = Score.ToString();
            }
        }

        /// <summary>
        /// Termine la partie, coupe la musique actuelle et
        /// la change par une nouvelle puis affiche le panel
        /// de score final.
        /// </summary>
        internal void EndGame()
        {
            _musicSource.Stop();
            _musicSource.clip = EndGameMusic;
            _musicSource.Play();

            _gameIsFinished = true;
            EndGamePanel.SetActive(true);
        }

        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}