﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TPNote2
{
    public enum FollowType
    {
        Update,
        FixedUpdate,
        LateUpdate
    }

    public class FollowPlayer : MonoBehaviour
    {
        public Transform Target;
        public FollowType FollowMethod;

        private Vector3 _offset;

        // Start is called before the first frame update
        void Start()
        {
            _offset = transform.position - Target.position;
        }

        // Update is called once per frame
        void Update()
        {
            if(FollowMethod == FollowType.Update)
            {
                Vector3 newPos = new Vector3(
                    Target.position.x + _offset.x,
                    transform.position.y,
                    Target.position.z + _offset.z
                );
                transform.position = newPos;
            }
        }

        void FixedUpdate()
        {
            if(FollowMethod == FollowType.FixedUpdate)
            {
                Vector3 newPos = new Vector3(
                    Target.position.x + _offset.x,
                    transform.position.y,
                    Target.position.z + _offset.z
                );
                transform.position = newPos;
            }
        }

        void LateUpdate()
        {
            if(FollowMethod == FollowType.LateUpdate)
            {
                Vector3 newPos = new Vector3(
                    Target.position.x + _offset.x,
                    transform.position.y,
                    Target.position.z + _offset.z
                );
                transform.position = newPos;
            }
        }
    }
}