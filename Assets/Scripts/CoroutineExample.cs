﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineExample : MonoBehaviour
{
    public float JumpDuration = 0.5f;
    private bool _isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && !_isMoving)
        {
            StartCoroutine(DoJump());
        }
    }

    private IEnumerator DoJump()
    {
        _isMoving = true;
        float time = 0;
        Vector3 positionDeDepart = transform.position,
            positionDeFin = transform.position + Vector3.up;
        while (time < JumpDuration)
        {
            transform.position = Vector3.Lerp(
                positionDeDepart, 
                positionDeFin, 
                time/ JumpDuration
            );
            time += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(2);

        time = 0;
        while (time < JumpDuration)
        {
            transform.position = Vector3.Lerp(
                positionDeFin,
                positionDeDepart,
                time / JumpDuration
            );
            time += Time.deltaTime;
            yield return null;
        }

        transform.position = positionDeDepart;
        _isMoving = false;
    }
}
