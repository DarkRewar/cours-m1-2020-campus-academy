﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementInput : MonoBehaviour
{
    public float MoveSpeed = 1;

    void Update()
    {
        Vector3 pos = transform.position;

        Vector3 movement = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")
        );

        Vector3 dir = transform.TransformDirection(movement);
        pos += MoveSpeed * Time.deltaTime * dir;

        transform.position = pos;

        float souris = Input.GetAxis("Mouse X");

        //if (Input.GetButtonDown("Jump"))
        //{
        //    transform.eulerAngles += 90 * Vector3.up;
        //}
    }
}
