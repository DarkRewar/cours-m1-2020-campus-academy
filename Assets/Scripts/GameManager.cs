﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region SINGLETON

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    #endregion

    public TMP_Text ScoreText;

    public Button ReloadBtn;

    public GameObject CoinPrefab;

    private int _score = 0;

    private void Start()
    {
        ReloadBtn.onClick.AddListener(ReloadScene);
    }

    public void Collect(GameObject cube)
    {
        ++_score;
        Destroy(cube);

        //ScoreText.text = "Score : " + _score;
        ScoreText.text = $"Score : {_score}";
    }

    public void Collect(CubeDestroyer cube)
    {
        Collect(cube.gameObject);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SpawnCoin()
    {
        Vector3 randPos = new Vector3(
            Random.Range(-10f, 10f),
            1f,
            Random.Range(-10f, 10f)
        );

        GameObject coin = Instantiate(
            CoinPrefab, 
            transform.position + randPos, 
            Quaternion.identity
        );
    }
}
