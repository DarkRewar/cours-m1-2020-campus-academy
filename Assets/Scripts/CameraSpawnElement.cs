﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSpawnElement : MonoBehaviour
{
    public Camera CameraLink;

    public GameObject Cursor;

    // Start is called before the first frame update
    void Start()
    {
        if (!CameraLink) CameraLink = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouse = Input.mousePosition;

        Ray ray = CameraLink.ScreenPointToRay(mouse);
        if(Cursor && Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            Cursor.SetActive(true);
            Cursor.transform.position = hit.point;

            if (Input.GetButton("Fire1"))
            {
                GameObject spawned = Instantiate(Cursor, hit.point, Quaternion.identity);
                Destroy(spawned, 3);
            }
        }
        else if(Cursor && Cursor.activeSelf)
        {
            Cursor.SetActive(false);
        }
    }
}
