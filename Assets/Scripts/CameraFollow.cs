﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public Vector3 Offset;

    public bool Smooth = false;
    public float SmoothSpeed = 1;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Smooth)
        {
            transform.position = Vector3.Lerp(
                transform.position,
                Target.position + Offset,
                Time.fixedDeltaTime * SmoothSpeed
            );
        }
        else
        {
            transform.position = Target.position + Offset;
        }
    }
}
