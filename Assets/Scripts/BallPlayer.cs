﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class BallPlayer : MonoBehaviour
{
    public float MoveSpeed = 1;
    public ForceMode ForceMode = ForceMode.Acceleration;
    public Rigidbody Rigidbody;

    public float JumpForce = 1;
    public ForceMode JumpForceMode = ForceMode.Impulse;

    public float CheckGround = 0.975f;
    private bool _isGrounded = true;

    public AudioSource AudioSource;

    public int Lifepoints = 50;
    public int MaxLifepoints = 50;
    public Slider Lifebar;

    private Vector3 _movement;

    // Start is called before the first frame update
    void Start()
    {
        this.Rigidbody = GetComponent<Rigidbody>();
        this.AudioSource = GetComponent<AudioSource>();

        /**
         * Permet de manipuler le volume audio de l'audio mixer
         * directement dans le code
         */
        //AudioSource.outputAudioMixerGroup.audioMixer.SetFloat("SFX_Volume", -6);

        Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void Update()
    {
        _movement = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")
        );

        // Saut
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            AudioSource.Play();
            _isGrounded = false;
            Rigidbody.AddForce(JumpForce * Vector3.up, JumpForceMode);
        }
    }

    private void FixedUpdate()
    {
        //VelocityMethod();

        Rigidbody.AddForce(
            MoveSpeed * _movement, 
            ForceMode
        );

        // Vérification si on touche de le sol.
        Ray ray = new Ray(Rigidbody.position, Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit hit, CheckGround))
        {
            _isGrounded = hit.collider != null && !hit.collider.isTrigger;
        }
        else _isGrounded = false;
    }

    private void LateUpdate()
    {
        Lifebar.value = (float)Lifepoints / MaxLifepoints;
    }

    private void VelocityMethod()
    {
        Rigidbody.velocity = MoveSpeed * _movement;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(Rigidbody.position, Rigidbody.position + (CheckGround * Vector3.down));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            --Lifepoints;

            if(Lifepoints <= 0)
            {
                GameManager.Instance.ReloadScene();
            }
        }
    }
}
