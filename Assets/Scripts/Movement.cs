﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float Amplitude = 1;
    public float RotationSpeed = 1;

    /**
     * Activation
     */

    private void Awake()
    {
        
    }

    private void OnEnable()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    /**
     * Desactivation
     */

    private void OnDisable()
    {

    }

    private void OnDestroy()
    {
        
    }

    /**
     * Chaque frame
     */

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = Mathf.Sin(Time.time) * Amplitude;
        transform.position = pos;

        Vector3 rot = transform.eulerAngles;
        rot.y += Time.deltaTime * RotationSpeed;
        transform.eulerAngles = rot;

        Vector3 scale = transform.localScale;
        scale = (1.5f + Mathf.Sin(Time.time)) * Vector3.one;
        transform.localScale = scale;
    }

    private void LateUpdate()
    {
        
    }

    private void FixedUpdate()
    {
        
    }
}
