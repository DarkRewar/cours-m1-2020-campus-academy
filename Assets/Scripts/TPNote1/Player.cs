﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TPNote1
{
    [RequireComponent(typeof(Rigidbody))]
    public class Player : MonoBehaviour
    {
        /// <summary>
        /// La vitesse du joueur appliquée à sa vélocité.
        /// </summary>
        public float MoveSpeed = 5;

        /// <summary>
        /// Il s'agit de la vitesse de suivi de la caméra.
        /// </summary>
        public float CameraFollowSpeed = 5;

        /// <summary>
        /// La vitesse de tir du vaisseau,
        /// le nombre de balle pouvant tirées à la seconde.
        /// </summary>
        public float ShotRate = 1;

        /// <summary>
        /// Le point d'origin d'où doit partir le tir.
        /// </summary>
        public Transform ShotOriginPoint;

        /// <summary>
        /// Le projectile que le joueur doit tirer.
        /// </summary>
        public GameObject ProjectilePrefab;

        /// <summary>
        /// La vitesse de la balle une fois tirée.
        /// </summary>
        public float ProjectileSpeed = 10;

        /// <summary>
        /// On enregistre l'input qu'execute le joueur.
        /// </summary>
        private Vector3 _movementInput;

        /// <summary>
        /// On stocke la caméra du joueur ici afin de le suivre.
        /// </summary>
        private Camera _camera;

        /// <summary>
        /// Le corps solide du joueur.
        /// </summary>
        private Rigidbody _rigidbody;

        /// <summary>
        /// La durée actuelle du cooldown (délai d'attente entre deux tirs).
        /// </summary>
        private float _shotCooldown = 0;

        // Start is called before the first frame update
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _camera = Camera.main;

            if (!_camera)
            {
                Debug.LogWarning("Attention ! Il n'y a pas de caméra attribuée au joueur.");
            }
        }

        // Update is called once per frame
        void Update()
        {
            UpdateOrientation();

            /**
             * On récupère les inputs du joueur afin
             * de pouvoir les utiliser dans le FixedUpdate()
             * Principalement car le joueur est un rigidbody
             */
            _movementInput.x = Input.GetAxis("Horizontal");
            _movementInput.z = Input.GetAxis("Vertical");

            /**
             * Gère l'action de tir du joueur.
             * Si le cooldown de tir a atteint 0 (ou moins) et que
             * le joueur appuie sur l'une des deux touches de tir voulue,
             * alors on appelle la méthode de tir "Shoot()".
             * 
             * Sinon, si le cooldown n'a pas encore atteint 0, on le décrémente.
             * 
             * Sinon, on ne fait rien.
             */
            if(_shotCooldown <= 0 && (Input.GetButton("Fire1") || Input.GetButton("Jump")))
            {
                Shoot();
            }
            else if(_shotCooldown > 0)
            {
                _shotCooldown -= Time.deltaTime;
            }
        }

        private void FixedUpdate()
        {
            UpdateCamera();

            /**
             * On applique l'input de mouvement du vaisseau
             * à la vélocité, en fonction de la vitesse de déplacement
             * définie dans l'inspector.
             */
            _rigidbody.velocity = MoveSpeed * _movementInput;
        }

        /// <summary>
        /// Mets à jour l'orientation du joueur en fonction
        /// de la position de la souris dans l'écran.
        /// </summary>
        internal void UpdateOrientation()
        {
            if (_camera)
            {
                Vector3 mouse = Input.mousePosition;
                
                /**
                 * Au lieu de passer par un Raycast, on peut directement
                 * définir la distance à laquelle on souhaite récupérer la position.
                 * Pour ce faire, on indique la distance entre la caméra et le vaisseau,
                 * et la place dans le Z.
                 * Puisque, par rapport à la caméra, le X et Y indiquent la position du curseur
                 * sur la camera, le Z indique la profondeur (la distance) par rapport à la caméra. 
                 */
                mouse.z = Mathf.Abs(_camera.transform.position.y - transform.position.y);
                Vector3 worldPoint = _camera.ScreenToWorldPoint(mouse);

                /**
                 * Une fois le point calculé dans le monde, on oriente 
                 * le vaisseau de manière à ce qu'il regarde vers ce point.
                 */
                transform.LookAt(worldPoint, Vector3.up);
            }
        }

        /// <summary>
        /// Cette méthode permet de mettre à jour le mouvement de la caméra.
        /// On la sort de l'Update() afin de ne pas surcharger cette dernière.
        /// </summary>
        internal void UpdateCamera()
        {
            /**
             * Si la camera existe, alors on peut appliquer
             * la logique de mouvement.
             */
            if (_camera)
            {
                _camera.transform.position = Vector3.Lerp(
                    _camera.transform.position,
                    transform.position + (10 * Vector3.up),
                    Time.fixedDeltaTime * CameraFollowSpeed
                );
            }
        }

        internal void Shoot()
        {
            /**
             * Pour calculer le cooldown entre deux dire,
             * on se sert du ShotRate afin de déterminer la durée
             * jusqu'au prochain tir.
             * Exemple, on veut faire trois tirs par seconde. On
             * a "ShotRate = 3". Le cooldown entre deux tirs sera donc
             * de 1/3 soit ~0.333s (~333ms).
             * Le "_shotCooldown" est alors décrémenté dans l'Update(),
             * une fois à 0, le joueur pourra tirer une nouvelle fois.
             */
            _shotCooldown = 1f / ShotRate;

            GameObject bullet = Instantiate(
                ProjectilePrefab, 
                ShotOriginPoint.position, 
                ShotOriginPoint.rotation
            );

            /**
             * On indique la puissance du tir. On multiplie donc
             * la ProjectileSpeed (sa vitesse) par le forward,
             * le vecteur indiquant la direction dans laquelle
             * doit aller le projectile, en avant.
             */
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.velocity = ProjectileSpeed * bullet.transform.forward;
            }

            /**
             * On sécurise le projectile en le détruisant s'il n'a rien touché
             * au bout de 10 secondes
             */
            Destroy(bullet, 10);
        }

        /// <summary>
        /// Si un ennemi entre en collision avec le joueur,
        /// on recharge la scène.
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}