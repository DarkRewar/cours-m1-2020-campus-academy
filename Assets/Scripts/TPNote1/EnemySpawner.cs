﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TPNote1
{
    public class EnemySpawner : MonoBehaviour
    {
        /// <summary>
        /// La référence du joueur dans la scène.
        /// </summary>
        public Player PlayerReference;

        /// <summary>
        /// L'ennemi à faire apparaître.
        /// Il est possible de faire apparaître directement un
        /// prefab complet en passant en référence l'un de ses composants.
        /// Cela permet d'instancier le GameObject du prefab et, en plus,
        /// de récupérer immédiatement le composant associé (ici Enemy).
        /// </summary>
        public Enemy EnemyPrefab;

        /// <summary>
        /// Le délai entre deux apparitions d'un ennemi.
        /// </summary>
        public float DelayBetweenEnemySpawns = 3;

        /// <summary>
        /// La distance d'apparition d'un ennemi par rapport au joueur.
        /// </summary>
        public float SpawnDistanceFromPlayer = 10;

        // Start is called before the first frame update
        IEnumerator Start()
        {
            do
            {
                SpawnEnemy();

                yield return new WaitForSeconds(DelayBetweenEnemySpawns);
            } while (true);
        }

        /// <summary>
        /// Méthode qui va faire apparaître mon ennemi et lui indiquer une cible.
        /// </summary>
        internal void SpawnEnemy()
        {
            Vector3 playerPosition = PlayerReference.transform.position;

            /**
             * Random.insideUnitCircle permet de récupérer aléatoirement
             * un point dans un espace 2D.
             */
            Vector2 randomPosition = Random.insideUnitCircle;

            /**
             * On normalize le point que l'on vient de récupérer afin qu'il
             * soit automatiquement placer autour d'un cercle radian.
             */
            randomPosition.Normalize();

            /**
             * On transforme le Vector2 en Vector3 selon notre espace de jeu.
             * Ici, le Vector2 récupérer utilise l'axe X et Y. Or, notre jeu 
             * en vue du dessus utilise l'axe X et Z.
             */
            Vector3 normalizedRandomPosition = new Vector3(randomPosition.x, 0, randomPosition.y);

            /**
             * On ajoute la position aléatoire obtenue à celle de notre joueur
             * afin que l'ennemi apparaîssent autour du vaisseau, dans un rayon
             * renseigné depuis l'inspector (SpawnDistanceFromPlayer).
             */
            Vector3 positionToSpawn = playerPosition + (SpawnDistanceFromPlayer * normalizedRandomPosition);

            /**
             * Une fois la position calculée, on fait apparaître notre ennemi
             * et on lui indique que sa cible est le joueur.
             * 
             * Ici, j'instancie directement l'ennemi passé en référence et je récupère
             * par la même occasion son composant Enemy.
             * Cela evite d'instancier un GameObject puis de devoir récupérer le composant
             * via un "GetComponent<Enemy>()".
             */
            Enemy enemy = Instantiate(EnemyPrefab, positionToSpawn, Quaternion.identity);
            enemy.Target = PlayerReference.transform;
        }

        /// <summary>
        /// Petit utilitaire de Unity qui permet d'afficher des informations dans l'éditeur.
        /// Ici, je souhaite m'assurer que les ennemis apparaitront correctement 
        /// autour du joueur selon le rayon que j'ai renseigné.
        /// 
        /// La méthode OnDrawGizmosSelected permet donc d'executer une action dans l'éditeur
        /// seulement quand je sélectionne l'objet sur lequel la méthode se trouve.
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(PlayerReference.transform.position, SpawnDistanceFromPlayer);
        }
    }
}