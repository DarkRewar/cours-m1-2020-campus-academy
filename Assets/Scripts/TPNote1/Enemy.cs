﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TPNote1
{
    [RequireComponent(typeof(Rigidbody))]
    public class Enemy : MonoBehaviour
    {
        /// <summary>
        /// Vitesse de déplacement de l'ennemi.
        /// </summary>
        public float MoveSpeed = 5;

        /// <summary>
        /// La cible vers laquelle l'ennemi doit se diriger.
        /// </summary>
        public Transform Target;

        /// <summary>
        /// Le corps solide de l'ennemi.
        /// </summary>
        private Rigidbody _rigidbody;

        // Start is called before the first frame update
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            /**
             * Si une cible est définie, alors l'ennemi 
             * s'orientera toujours vers elle.
             */
            if (Target)
            {
                transform.LookAt(Target, Vector3.up);
            }
        }

        private void FixedUpdate()
        {
            /**
             * Si une cible est définie, alors l'ennemi 
             * ira la rejoindre en avancant tout droit.
             */
            if (Target)
            {
                _rigidbody.velocity = MoveSpeed * transform.forward;
            }
        }

        /// <summary>
        /// Si un projectile entre en collision avec l'ennemi,
        /// les deux sont détruits.
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("PlayerProjectile"))
            {
                Destroy(collision.gameObject);
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Si un projectile ayant un collider en "isTrigger" touche
        /// l'ennemi, les deux sont détruits.
        /// </summary>
        /// <param name="collider"></param>
        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.CompareTag("PlayerProjectile"))
            {
                Destroy(collider.gameObject);
                Destroy(gameObject);
            }
        }
    }
}