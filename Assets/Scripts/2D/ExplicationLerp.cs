﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplicationLerp : MonoBehaviour
{
    [Range(0f, 1f)]
    public float Lerp = 0;

    private Vector3 _origin;

    private bool _reverse = false;

    // Start is called before the first frame update
    void Start()
    {
        float lerpFloat = Mathf.Lerp(0, 128, 0f);

        _origin = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lerpPos = Vector3.Lerp(_origin, _origin + new Vector3(3, 3, 0), Lerp);

        transform.position = lerpPos;

        if (!_reverse) Lerp += Time.deltaTime;
        else Lerp -= Time.deltaTime;

        if(Lerp >= 1 && !_reverse)
        {
            Lerp = 1;
            _reverse = true;
        }
        else if(Lerp <= 0 && _reverse)
        {
            Lerp = 0;
            _reverse = false;
        }
    }
}
