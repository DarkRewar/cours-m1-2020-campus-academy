﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed = 2;

    public float JumpForce = 2;
    public ForceMode2D JumpForceMode;

    public float CameraFollowSpeed = 2;
    public float CameraForwardPreview = 2;
    public Camera Camera;
    public SpriteRenderer Renderer;
    public Rigidbody2D Rigidbody;

    private float _inputHorizontal;

    private bool _isGrounded = false;
    private bool _secondJumpDone = false;

    // Start is called before the first frame update
    void Start()
    {
        Camera = Camera.main;
        Renderer = GetComponent<SpriteRenderer>();
        Rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _inputHorizontal = Input.GetAxis("Horizontal");

        // Orientation du joueur dans la direction du mouvement.
        if(_inputHorizontal < 0)
        {
            Renderer.flipX = true;
        }
        else if(_inputHorizontal > 0)
        {
            Renderer.flipX = false;
        }

        if (Input.GetButtonDown("Jump") && (_isGrounded || !_secondJumpDone))
        {
            if (!_isGrounded) _secondJumpDone = true;

            _isGrounded = false;

            Rigidbody.AddForce(
                JumpForce * Vector2.up, 
                JumpForceMode
            );
        }
    }

    private void FixedUpdate()
    {
        Vector2 velocity = Rigidbody.velocity;
        velocity.x = MoveSpeed * _inputHorizontal;
        Rigidbody.velocity = velocity;

        if(Physics2D.OverlapCircle(Rigidbody.position, 0.1f, LayerMask.GetMask("Ground")))
        {
            _secondJumpDone = false;
            _isGrounded = true;
        }
        else
        {
            _isGrounded = false;
        }
    }

    private void LateUpdate()
    {
        Vector3 newPos = transform.position;
        newPos.z = -10;
        newPos.y += 0.5f;
        newPos.x += CameraForwardPreview * _inputHorizontal;

        // Fluidification du mouvement de la caméra
        newPos = Vector3.Lerp(
            Camera.transform.position, 
            newPos, 
            CameraFollowSpeed * Time.deltaTime
        );

        Camera.transform.position = newPos;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(Rigidbody.position, 0.1f);
    }
}
